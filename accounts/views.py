from msilib.schema import ListView
from urllib import request
from django.shortcuts import render, redirect
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from django.contrib.auth import login

# Create your views here.


def signup(request):
    if request.method == "POST":  # if the request method is "POST", then
        form = UserCreationForm(request.POST)  # create a form
        if form.is_valid():  # if the form is valid, then
            username = request.POST.get(
                "username"
            )  # the 'username'value from the request.POST dictionary
            password = request.POST.get(
                "password1"
            )  # the 'password1' value from the request.POST dictionary
            user = User.objects.create_user(
                username=username,
                password=password,
            )
            user.save()  # save the user object
            login(
                request, user
            )  # login the user with the request and user object
            return redirect("home")  # return a redirect to the "home" path
    else:  # otherwise,
        form = UserCreationForm(request.POST)
        context = {  # context = dictionary with key
            "form": form,  # "form" and value form
        }
    return render(
        request, "registration/signup.html", context
    )  # return render with request, "registration/signup.html", and the context


