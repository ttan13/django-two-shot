from django.shortcuts import redirect
from django.views.generic.base import TemplateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView


from receipts.models import Receipt, Account, ExpenseCategory

# Create your views here.

class AccountCreateView(CreateView):
    model = Account
    template_name = "accounts/create.html"
    fields = ["name", "number"]

    def form_valid(self, form):         
        item = form.save(commit=False)          # automatically assign the current user
        item.owner = self.request.user  # to the appropriate User proeprty
        item.save()
        return redirect("list_accounts")


class ExpenseCategoryCreateView(LoginRequiredMixin, CreateView):    # To access any list view, a person must be logged in
    model = ExpenseCategory
    template_name = "expense_categories/create.html"
    fields = ["name"]

    def form_valid(self, form):         
            item = form.save(commit=False)          # automatically assign the current user
            item.owner = self.request.user  # to the appropriate User proeprty
            item.save()
            return redirect("list_categories")    


class ReceiptCreateView(CreateView):
    model = Receipt
    template_name = "receipts/create.html"
    fields = ["vendor", "total", "tax", "date", "purchaser","category", "account"]

    def form_valid(self, form):         
                item = form.save(commit=False)          # automatically assign the current user
                item.purchaser = self.request.user  # to the appropriate User proeprty
                item.save()
                return redirect("home") 




class AccountListView(LoginRequiredMixin, ListView):    # To access any list view, a person must be logged in
    model = Account
    template_name = "accounts/list.html"

    def get_queryset(self): #display the data in tables. Be sure to filter the data for only the current user
        return Account.objects.filter(owner=self.request.user)    # Refer to the model and user_property that is a foreign key to the object


class ExpenseCategoryListView(LoginRequiredMixin, ListView):    # To access any list view, a person must be logged in
    model = ExpenseCategory
    template_name = "expense_categories/list.html"

    def get_queryset(self):
        return ExpenseCategory.objects.filter(owner=self.request.user)


class ReceiptListView(LoginRequiredMixin, ListView):    # To access any list view, a person must be logged in
    model = Receipt
    template_name = "receipts/list.html"

    def get_queryset(self):
        return Receipt.objects.filter(purchaser=self.request.user,)
    